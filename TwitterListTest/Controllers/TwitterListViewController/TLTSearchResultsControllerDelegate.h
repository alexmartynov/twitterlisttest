//
//  TLTSearchResultsControllerDelegate.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/18/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TLTSearchResultsController;

@protocol TLTSearchResultsControllerDelegate <NSObject>

@required
- (void) searchResultsController:(TLTSearchResultsController*)searchController didUpdatePosts:(NSArray*)newPosts;

- (void) searchResultsController:(TLTSearchResultsController*)searchController didFailWithError:(NSError*)error;

@end
