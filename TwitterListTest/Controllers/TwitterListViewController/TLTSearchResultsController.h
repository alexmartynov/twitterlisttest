//
//  TLTSearchResultsController.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/18/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TLTSearchResultsControllerDelegate.h"

@interface TLTSearchResultsController : NSObject

+ (instancetype)controllerWithQuery:(NSString *)aQuery;

- (instancetype)initWithQuery:(NSString *)aQuery;

@property (nonatomic, assign) id<TLTSearchResultsControllerDelegate> delegate;

- (void)loadUpdates;

- (void)loadMore;

- (void)cancel;

@end
