//
//  TLTTwitterListViewController.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/8/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTimelineViewController.h"

#import "TLTTwitterPostCell.h"
#import "TLTTwitterPost.h"
#import "TLTSystemVersion.h"
#import "TLTCountdownLabel.h"
#import "TLTCellHeighCalculator.h"
#import "TLTUserSettings.h"
#import "TLTImageLoader.h"
#import "TLTSearchResultsController.h"

static NSString * const kTLTTwitterQuery = @"mailru";
static NSString * const kTLTTwitterPostCellReusableId = @"kTLTTwitterPostCell";
static const NSTimeInterval kTLTRefreshInterval = 60.0;

@interface TLTTimelineViewController () <UITableViewDataSource, UITableViewDelegate, TLTSearchResultsControllerDelegate> {
    TLTImageLoader *_imageLoader;
}

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet TLTCountdownLabel *countdownLabel;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, strong) TLTSearchResultsController *searchResultsController;
@property (nonatomic, strong) TLTUserSettings *userSettings;
@property (nonatomic, strong) NSArray *twitterPosts;
@property (nonatomic, strong) TLTCellHeighCalculator *cellHeighCalculatorForIOS7;

@end

@implementation TLTTimelineViewController

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.cellHeighCalculatorForIOS7 = [TLTCellHeighCalculator new];
        self.userSettings = [TLTUserSettings new];
        self.searchResultsController = [TLTSearchResultsController controllerWithQuery:kTLTTwitterQuery];
        self.searchResultsController.delegate = self;
        _imageLoader = [TLTImageLoader new];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = kTLTTwitterQuery;
    
    if (TLT_SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        self.tableView.estimatedRowHeight = 80;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    [self.tableView registerNib: [UINib nibWithNibName:@"TLTTwitterPostCell" bundle:nil]
         forCellReuseIdentifier: kTLTTwitterPostCellReusableId];
    
    self.countdownLabel.hidesWhenStopped = YES;
    
    [self addPullToRefreshControl];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self reloadData];
    [self startReloadPosts];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.countdownLabel stop];
    
    [self.searchResultsController cancel];
}

#pragma mark - Actions

- (void)pullToRefreshAction {
    [self startReloadPosts];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.twitterPosts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TLTTwitterPostCell *cell = (TLTTwitterPostCell*)[tableView dequeueReusableCellWithIdentifier:kTLTTwitterPostCellReusableId];
    
    [cell configure:self.twitterPosts[indexPath.row] imageLoader:_imageLoader displayProfileImage:self.userSettings.showUserAvatar];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (TLT_SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        return [self.cellHeighCalculatorForIOS7 heightForCellInTableView:tableView
                                                             atIndexPath:indexPath
                                                      forReuseIdentifier:kTLTTwitterPostCellReusableId
                                                        configureHandler:^(UITableViewCell *cell) {
                                                            TLTTwitterPostCell *twitterCell = (TLTTwitterPostCell *)cell;
                                                            [twitterCell configure:self.twitterPosts[indexPath.row]
                                                                       imageLoader:nil
                                                               displayProfileImage:NO];
                                                        }];
    }
    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger lastSectionIndex = [tableView numberOfSections] - 1;
    NSInteger lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    if ((indexPath.section == lastSectionIndex) && (indexPath.row == lastRowIndex)) {
        [self loadMorePosts];
    }
}

#pragma mark - TLTCountdownLabelDelegate

- (void) countdownLabelDidFinishCounting:(TLTCountdownLabel*)label {
    NSLog(@"%s. Updating tweets", __func__);
    [self startReloadPosts];
}

#pragma mark - TLTSearchResultsControllerDelegate

- (void) searchResultsController:(TLTSearchResultsController*)searchController didUpdatePosts:(NSArray*)newPosts {
    NSLog(@"%s. New number of posts = %ld", __func__, (unsigned long)[newPosts count]);
    self.twitterPosts = newPosts;
    [self reloadData];
}

- (void) searchResultsController:(TLTSearchResultsController*)searchController didFailWithError:(NSError*)error {
    NSLog(@"%s. Error = %@", __func__, error);
    [self showError:error];
}

#pragma mark - private

- (void)addPullToRefreshControl {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blueColor];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString: @"Loading..."];
    
    [refreshControl addTarget:self
                       action:@selector(pullToRefreshAction)
             forControlEvents:UIControlEventValueChanged];
    
    self.refreshControl = refreshControl;
    
    self.refreshControl.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.tableView addSubview:self.refreshControl];
}

- (void)startReloadPosts {
    [self.countdownLabel stop];
    [self.refreshControl beginRefreshing];
    [self.searchResultsController loadUpdates];
}

- (void)reloadData {
    [self.countdownLabel startCountdownWithTimeInterval:kTLTRefreshInterval];
    [self.cellHeighCalculatorForIOS7 clearCache];
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

- (void)showError:(NSError*)err{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Please check your network connection"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        alert = nil;
        
        [self.refreshControl endRefreshing];
        [self.countdownLabel startCountdownWithTimeInterval:kTLTRefreshInterval];
    });
}

- (void)loadMorePosts {

    [self.searchResultsController loadMore];
}

@end
