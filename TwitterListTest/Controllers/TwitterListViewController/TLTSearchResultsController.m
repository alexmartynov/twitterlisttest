//
//  TLTSearchResultsController.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/18/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTSearchResultsController.h"

#import "TLTPostsCache.h"
#import "TLTTwitterPost.h"
#import "TLTTwitterService.h"
#import "NSMutableArray_AddObjectsToFront.h"

static const NSUInteger kTLTSearchResultsControllerSearchSize = 20;

@interface TLTSearchResultsController() {
    TLTTwitterService *_twitterService;
    TLTPostsCache *_postsCache;
    BOOL _loadedFromCache;
}

@property (nonatomic, copy) NSString *query;
@property (nonatomic, strong) TLTTwitterServiceTask *loadUpdatesTask;
@property (nonatomic, strong) TLTTwitterServiceTask *loadMoreTask;

@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) NSObject *syncObj;

@end

@implementation TLTSearchResultsController

+ (instancetype)controllerWithQuery:(NSString *)aQuery {
    return [[self alloc] initWithQuery:aQuery];
}

- (instancetype)initWithQuery:(NSString *)aQuery {
    if (self = [super init]) {
        _twitterService = [TLTTwitterService new];
        self.query = aQuery;
        self.searchResults = [NSMutableArray new];
        self.syncObj = [NSObject new];
        _postsCache = [TLTPostsCache new];
    }
    return self;
}

#pragma mark - public

- (void)loadUpdates {
    
    //todo:: cleanup
    if (!_loadedFromCache) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            _loadedFromCache = YES;
            NSArray *cachedPosts = [_postsCache posts];
            
            if ([cachedPosts count] > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate searchResultsController:self didUpdatePosts:cachedPosts];
                });
            }
            
            [self loadUpdatesInternal];
        });
    }
    else {
        [self loadUpdatesInternal];
    }
}

- (void)loadMore {
    TLTTwitterSearchQuery *query = [self queryForLoadMore];
    
    [self.loadMoreTask cancel];
    self.loadMoreTask = nil;
    self.loadMoreTask = [_twitterService searchTaskWithQuery:query
                                           completionHandler:^(NSArray *posts, NSError *err) {
                                                   if (err != nil) {
                                                       [self handleError:err];
                                                   } else {
                                                       [self handleLoadMoreDidComplete:posts];
                                                   }
                                               }];
    
    [self.loadMoreTask start];
}

- (void)cancel {
    [self.loadUpdatesTask cancel];
    self.loadUpdatesTask = nil;
    
    [self.loadMoreTask cancel];
    self.loadMoreTask = nil;
}

#pragma mark - private

- (void)loadUpdatesInternal {
    TLTTwitterSearchQuery *query = [self queryForLoadUpdates];
    
    [self.loadUpdatesTask cancel];
    self.loadUpdatesTask = nil;
    self.loadUpdatesTask = [_twitterService searchTaskWithQuery:query
                                              completionHandler:^(NSArray *posts, NSError *err) {
                                                  if (err != nil) {
                                                      [self handleError:err];
                                                  } else {
                                                      [self handleLoadUpdateDidComplete:posts];
                                                  }
                                              }];
    
    [self.loadUpdatesTask start];
}

- (TLTTwitterSearchQuery *)queryForLoadUpdates {
    TLTTwitterPost *firstPost = [self.searchResults firstObject];
    if (firstPost == nil) {
        return [TLTTwitterSearchQuery queryWithSearchString:self.query numberOfResults:kTLTSearchResultsControllerSearchSize];
    }
                
    return [TLTTwitterSearchQuery queryWithSearchString:self.query
                                                  maxId:TLTTWITTER_SEARCH_NO_ID
                                                sinceId:firstPost.postId
                                        numberOfResults:kTLTSearchResultsControllerSearchSize];
}

- (TLTTwitterSearchQuery *)queryForLoadMore {
    TLTTwitterPost *lastPost = [self.searchResults lastObject];
    if (lastPost == nil) {
        return [TLTTwitterSearchQuery queryWithSearchString:self.query  numberOfResults:kTLTSearchResultsControllerSearchSize];
    }
    
    return [TLTTwitterSearchQuery queryWithSearchString:self.query
                                                  maxId:lastPost.postId
                                                sinceId:TLTTWITTER_SEARCH_NO_ID
                                        numberOfResults:kTLTSearchResultsControllerSearchSize];
}

- (void)handleLoadUpdateDidComplete:(NSArray*)newPosts{

    NSArray *updatedPosts = nil;
    
    @synchronized(self.syncObj) {
        [_postsCache add:newPosts];
        [self.searchResults tlt_addObjectsToFront:newPosts];
        
        updatedPosts = [NSArray arrayWithArray: self.searchResults];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate searchResultsController:self didUpdatePosts:updatedPosts];
    });
}

- (void)handleLoadMoreDidComplete:(NSArray*)newPosts{
    
    NSArray *updatedPosts = nil;
    
    @synchronized(self.syncObj) {
        [_postsCache add:newPosts];
        [self.searchResults addObjectsFromArray:newPosts];
        
        updatedPosts = [NSArray arrayWithArray: self.searchResults];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate searchResultsController:self didUpdatePosts:updatedPosts];
    });
}

- (void)handleError:(NSError*)err{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate searchResultsController:self didFailWithError:err];
    });
}

@end
