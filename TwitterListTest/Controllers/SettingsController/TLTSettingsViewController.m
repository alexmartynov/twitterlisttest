//
//  TLTSettingsViewController.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTSettingsViewController.h"

#import "TLTUserSettings.h"
#import "TLTSwitchTableViewCell.h"

static NSString * const kTLTSettingsViewControllerCellReusableId = @"TLTSwitchTableViewCell";

@interface TLTSettingsViewController ()

@property (nonatomic, strong) TLTUserSettings *userSettings;

@end

@implementation TLTSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Settings";
    
    [self.tableView registerNib: [UINib nibWithNibName:@"TLTSwitchTableViewCell" bundle:nil]
         forCellReuseIdentifier: kTLTSettingsViewControllerCellReusableId];
    
    self.userSettings = [TLTUserSettings new];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TLTSwitchTableViewCell *cell = (TLTSwitchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kTLTSettingsViewControllerCellReusableId];
    
    cell.switchTitle.text = @"User Avatar";
    cell.switchControl.on = self.userSettings.showUserAvatar;
    cell.valueChangedHandler = ^void(BOOL newValue) {
        self.userSettings.showUserAvatar = newValue;
    };
    
    return cell;
}

@end
