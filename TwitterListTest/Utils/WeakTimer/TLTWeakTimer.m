//
//  TLTWeakTimer.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/21/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTWeakTimer.h"

//INFO: based on https://github.com/th-in-gs/THIn/blob/master/THIn/THIn.m
@implementation TLTWeakTimer {
    void(^_block)(void);
    CFRunLoopTimerRef _runLoopTimer;
}

+ (instancetype)timerWithDelay:(NSTimeInterval)delay block:(void (^)(void))block {
    return [[self alloc] initWithDelay:delay block:block];
}

+ (instancetype)timerWithRepeatingDelay:(NSTimeInterval)delay block:(void (^)(void))block {
    return [[self alloc] initWithRepeatingDelay:delay block:block];
}

- (instancetype)initWithDelay:(NSTimeInterval)delay block:(void (^)(void))block {
    return [self _initWithFireTime:CFAbsoluteTimeGetCurrent() + delay repeatInterval:0 do:block];
}

- (instancetype)initWithRepeatingDelay:(NSTimeInterval)delay block:(void (^)(void))block {
    return [self _initWithFireTime:CFAbsoluteTimeGetCurrent() + delay repeatInterval:delay do:block];
}

- (instancetype)_initWithFireTime:(CFAbsoluteTime)fireTime repeatInterval:(CFTimeInterval)interval do:(void (^)(void))block
{
    _block = block;
    
    __weak TLTWeakTimer *wSelf = self;
    _runLoopTimer = CFRunLoopTimerCreateWithHandler(kCFAllocatorDefault,
                                                    fireTime,
                                                    interval,
                                                    0,
                                                    0,
                                                    ^(CFRunLoopTimerRef timer) {
                                                        [wSelf _fire];
                                                    });
    CFRunLoopAddTimer(CFRunLoopGetMain(), _runLoopTimer, kCFRunLoopCommonModes);
    
    return self;
}

- (void)dealloc
{
    [self invalidate];
}

#pragma mark - private

- (void)_fire
{
    if(_block) {
        _block();
        if(_runLoopTimer && !CFRunLoopTimerDoesRepeat(_runLoopTimer)) {
            [self invalidate];
        }
    }
}

- (void)invalidate
{
    if(_runLoopTimer) {
        if(CFRunLoopTimerIsValid(_runLoopTimer)) {
            CFRunLoopTimerInvalidate(_runLoopTimer);
        }
        CFRelease(_runLoopTimer);
        _runLoopTimer = nil;
        _block = nil;
    }
}

@end
