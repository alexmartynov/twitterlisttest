//
//  TLTWeakTimer.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/21/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLTWeakTimer : NSObject

+ (instancetype)timerWithDelay:(NSTimeInterval)delay block:(void (^)(void))block;

+ (instancetype)timerWithRepeatingDelay:(NSTimeInterval)delay block:(void (^)(void))block;

- (instancetype)initWithDelay:(NSTimeInterval)delay block:(void (^)(void))block;

- (instancetype)initWithRepeatingDelay:(NSTimeInterval)delay block:(void (^)(void))block;

- (void)invalidate;

@end
