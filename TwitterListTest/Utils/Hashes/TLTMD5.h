//
//  TLTMD5.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/15/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLTMD5 : NSObject

+ (NSString *) md5:(NSString *) str;

@end
