//
//  TLTMD5.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/15/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTMD5.h"

#import <CommonCrypto/CommonDigest.h>

@implementation TLTMD5

+ (NSString *) md5:(NSString *) str
{
    const char *cStr = [str UTF8String];
    
    unsigned char digest[16];
    CC_MD5 (cStr, (CC_LONG)strlen(cStr), digest);
    
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [result appendFormat:@"%02x", digest[i]];
    }
    
    return result;
}

@end
