//
//  TLTSystemFolders.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTSystemFolders.h"

@implementation TLTSystemFolders

+ (NSString *) cachesPath {
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    
    NSAssert(cachesPath != nil, @"Cache path is nil");
    
    return cachesPath;
}

+ (NSString *) libraryPath {
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    
    NSAssert(libraryPath != nil, @"Library path is nil");
    
    return libraryPath;
}

@end
