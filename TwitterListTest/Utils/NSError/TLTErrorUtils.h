//
//  NSErrorUtils.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/20/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TLTSET_NSERROR_PTR(err, val) \
if ((err != nil)) { (*err) = (val); }
