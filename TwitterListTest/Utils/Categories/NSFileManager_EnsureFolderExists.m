//
//  NSFileManager_EnsureFolderExists.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "NSFileManager_EnsureFolderExists.h"

@implementation NSFileManager(EnsureFolderExists)

- (void) tlt_enshureDirectoryExists:(NSString*)path {
    BOOL isDir = NO;
    if (![self fileExistsAtPath:path isDirectory:&isDir] || !isDir) {
        NSError *err = nil;
        [self createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&err];
        NSAssert(err == nil, @"Cache dir creation failed. Error %@", err);
    }
}

@end
