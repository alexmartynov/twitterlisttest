//
//  NSFileManager_EnsureFolderExists.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager(EnsureFolderExists)

- (void) tlt_enshureDirectoryExists:(NSString*)path;

@end
