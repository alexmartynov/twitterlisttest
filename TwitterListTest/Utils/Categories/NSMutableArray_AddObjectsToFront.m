//
//  NSMutableArray_AddObjectsToFront.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/21/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "NSMutableArray_AddObjectsToFront.h"

@implementation NSMutableArray(AddObjectsToFront)

- (void) tlt_addObjectsToFront:(NSArray *)objects {
    if (objects == nil || [objects count] == 0) {
        return;
    }
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange: NSMakeRange(0,[objects count])];
    [self insertObjects:objects atIndexes:indexes];
}

@end
