//
//  NSMutableArray_AddObjectsToFront.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/21/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray(AddObjectsToFront)

- (void) tlt_addObjectsToFront:(NSArray *)objects;

@end
