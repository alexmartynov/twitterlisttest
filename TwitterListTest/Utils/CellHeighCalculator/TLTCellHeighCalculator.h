//
//  TLTCellHeighCalculator.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLTCellHeighCalculator : NSObject

- (void)clearCache;

- (CGFloat)heightForCellInTableView:(UITableView *)tableView
                        atIndexPath:(NSIndexPath *)indexPath
                 forReuseIdentifier:(NSString *)reuseIdentifier
                   configureHandler:(void (^)(UITableViewCell *))configureHandler;

@end
