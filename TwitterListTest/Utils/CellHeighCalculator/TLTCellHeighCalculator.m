//
//  TLTCellHeighCalculator.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTCellHeighCalculator.h"

@interface TLTCellHeighCalculator()

@property (nonatomic, strong) NSMutableDictionary *cachedCellHeights;

@end

@implementation TLTCellHeighCalculator

- (instancetype)init {
    if (self = [super init]) {
        self.cachedCellHeights = [NSMutableDictionary new];
    }
    
    return self;
}

#pragma mark - public

- (void)clearCache {
    [self.cachedCellHeights removeAllObjects];
}

- (CGFloat)heightForCellInTableView:(UITableView *)tableView
                        atIndexPath:(NSIndexPath *)indexPath
                 forReuseIdentifier:(NSString *)reuseIdentifier
                   configureHandler:(void (^)(UITableViewCell *))configureHandler {
    //INFO: idea - http://www.raywenderlich.com/87975/dynamic-table-view-cell-height-ios-8-swift
    //INFO: right now TLTCellHeighCalculator supports only one kind of cell, because sizingCell is static variable
    
    static UITableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    });
    
    NSNumber *cachedheightObj = self.cachedCellHeights[indexPath];
    if (cachedheightObj != nil) {
        return [cachedheightObj floatValue];
    }
    
    configureHandler(sizingCell);
    
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    CGFloat result = size.height + 1.0f; // Add 1.0f for the cell separator height
    
    self.cachedCellHeights[indexPath] = @(result);
    
    return result;
}

@end
