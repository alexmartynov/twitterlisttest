//
//  TLTTwitterCache.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTPostsCache.h"

#import "FMDB.h"
#import "TLTSystemFolders.h"
#import "NSFileManager_EnsureFolderExists.h"

static const int kTLTPostsCacheMaxCachedPosts = 200;

@interface TLTPostsCache() {
    FMDatabase *_database;
    NSObject *_syncObj;
}

@end

@implementation TLTPostsCache

- (instancetype) init{
    if (self = [super init]) {
        _syncObj = [NSObject new];
        [self openDatabase];
        [self createTables];
        [self createIndexes];
    }
    return self;
}

- (void) dealloc {
    [self closeDatabase];
}

#pragma mark - public

- (NSArray *)posts {
    @synchronized(_syncObj) {
        NSMutableArray *result = [NSMutableArray new];
        FMResultSet *resultSet = [_database executeQuery:@"SELECT p.id AS postId, p.text as postText, p.date as postDate, \
    u.id as userId, u.name as userName, u.screenName as userScreenName, u.awatarURL as userAwatarURL \
    FROM Posts p JOIN Users u ON p.userId == u.id ORDER BY postId"];
        
        while ([resultSet next]) {
            
            int64_t userId = [resultSet longLongIntForColumn:@"userId"];
            NSString *userName = [resultSet stringForColumn:@"userName"];
            NSString *userScreenName = [resultSet stringForColumn:@"userScreenName"];
            NSString *userAwatarURL = [resultSet stringForColumn:@"userAwatarURL"];
            TLTTwitterUser *user = [TLTTwitterUser userWithId:userId
                                                         name:userName
                                                   screenName:userScreenName
                                                    awatarURL:[NSURL URLWithString:userAwatarURL]];
            
            int64_t postId = [resultSet longLongIntForColumn:@"postId"];
            NSString *postText = [resultSet stringForColumn:@"postText"];
            NSTimeInterval postDate = [resultSet doubleForColumn:@"postDate"];
            TLTTwitterPost *post = [TLTTwitterPost postWithId:postId
                                                         text:postText
                                                         date:[NSDate dateWithTimeIntervalSinceReferenceDate:postDate]
                                                         user:user];
            
            [result addObject:post];
        }
        
        [resultSet close];
        
        return result;
    }
}

- (void) add:(NSArray *)posts {
    
    @synchronized(_syncObj) {
        if ([self numberOfPosts] + [posts count] > kTLTPostsCacheMaxCachedPosts) {
            [self clear];
        }
        
        [_database beginTransaction];
        
        for (TLTTwitterPost *post in posts) {
            [self addPostIfNotExists:post];
        }
        
        [_database commit];
    }
}

#pragma mark - private

+ (NSString *) databasePath {
    return [[TLTSystemFolders cachesPath] stringByAppendingPathComponent:@"PostsCache"];
}

- (void) openDatabase {
    NSString *dbFolder = [TLTPostsCache databasePath];
    
    [[NSFileManager new] tlt_enshureDirectoryExists:dbFolder];
    NSString *dbFile = [dbFolder stringByAppendingPathComponent:@"db.sqlite"];
    
    _database = [FMDatabase databaseWithPath:dbFile];
    if (![_database open]) {
        NSAssert(NO, @"Failed to open database %@", dbFile);
        _database = nil;
    }
}

- (void) closeDatabase {
    _database = nil;
}

- (void) createTables {
    NSString *sql = @"CREATE TABLE IF NOT EXISTS Posts (id BIGINT, text TEXT, date DOUBLE, userId BIGINT);"
    "CREATE TABLE IF NOT EXISTS Users (id BIGINT, name TEXT, screenName TEXT, awatarURL TEXT);";
    
    if (![_database executeStatements:sql]) {
        NSLog(@"Tables creation failed");
        NSAssert(NO, @"Tables creation failed");
    }
}

- (void) createIndexes {
    NSString *sql = @"CREATE INDEX IF NOT EXISTS posts_id_idx ON Posts (id ASC);"
    "CREATE INDEX IF NOT EXISTS users_id_idx ON Users (id ASC);";
    
    if (![_database executeStatements:sql]) {
        NSLog(@"Indexes creation failed");
        NSAssert(NO, @"Indexes creation failed");
    }
}

- (void) clear {
    NSString *sql = @"DELETE FROM Posts;DELETE FROM Users;";
    
    [_database beginTransaction];
    
    if (![_database executeStatements:sql]) {
        NSLog(@"Clear failed");
        NSAssert(NO, @"Clear failed");
    }
    
    [_database commit];
}

- (int) numberOfPosts {
    int result = 0;
    FMResultSet *resultSet = [_database executeQuery:@"SELECT count(*) FROM Posts"];
    while ([resultSet next]) {
        result = ([resultSet intForColumnIndex:0] > 0);
    }
    
    [resultSet close];
    
    return result;
}

- (void) addPostIfNotExists:(TLTTwitterPost *)post {
    [self addUserIfNotExists:post.user];
    
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:@(post.postId), @"id",
                              post.text, @"text",
                              @([post.date timeIntervalSinceReferenceDate]), @"date",
                              @(post.user.userId), @"userId", nil];
    BOOL result = [_database executeUpdate:@"INSERT INTO Posts (id, text, date, userId) VALUES (:id, :text, :date, :userId)"
                       withParameterDictionary:argsDict];
    
    if (!result) {
        NSLog(@"addPostIfNotExists failed");
    }
    NSAssert(result, @"addPostIfNotExists failed");
}

- (void) addUserIfNotExists:(TLTTwitterUser *)user {
    
    if ([self isUserExists:user]) {
        return;
    }
    
    NSString *name = user.name ? user.name : @"";
    NSString *screenName = user.screenName ? user.screenName : @"";
    NSString *awatarURL = user.awatarURL ? user.awatarURL.absoluteString : @"";
    
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:@(user.userId), @"id",
                              name, @"name",
                              screenName, @"screenName",
                              awatarURL, @"awatarURL", nil];
    BOOL result = [_database executeUpdate:@"INSERT INTO Users (id, name, screenName, awatarURL) VALUES (:id, :name, :screenName, :awatarURL)"
                       withParameterDictionary:argsDict];
    
    if (!result) {
        NSLog(@"addUserIfNotExists failed");
    }
    NSAssert(result, @"addUserIfNotExists failed");
}

- (BOOL) isUserExists:(TLTTwitterUser *)user {
    BOOL result = NO;
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:@(user.userId), @"userId", nil];

    FMResultSet *resultSet = [_database executeQuery:@"SELECT count(*) FROM Users WHERE id = :userId"
                                 withParameterDictionary:argsDict];
    while ([resultSet next]) {
        result = ([resultSet intForColumnIndex:0] > 0);
    }
    
    [resultSet close];
    
    return result;
}

@end
