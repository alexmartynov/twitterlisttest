//
//  TLTTwitterCache.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterPost.h"

@interface TLTPostsCache : NSObject

- (NSArray *)posts;

- (void)add:(NSArray *)posts;

@end
