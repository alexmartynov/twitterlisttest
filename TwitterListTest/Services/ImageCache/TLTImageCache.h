//
//  TLTImageCache.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/15/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLTImageCache : NSObject

- (UIImage *)imageForURL:(NSURL *)url;

- (void)addImage:(UIImage *)image forURL:(NSURL *)url;

@end
