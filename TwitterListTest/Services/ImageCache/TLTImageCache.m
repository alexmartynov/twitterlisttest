//
//  TLTImageCache.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/15/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTImageCache.h"

#import "TLTMD5.h"
#import "TLTSystemFolders.h"
#import "NSFileManager_EnsureFolderExists.h"

@interface TLTImageCache() {
    NSObject *_syncObj;
    NSString *_cacheFolder;
    NSFileManager *_fileManager;
}

@end

@implementation TLTImageCache

- (instancetype) init {
    if (self = [super init]) {
        _syncObj = [NSObject new];
        _fileManager = [NSFileManager new];
        _cacheFolder = [TLTImageCache cacheFolderPath];
    }
    
    return self;
}

#pragma mark - public

- (UIImage *)imageForURL:(NSURL *)url {
    @synchronized(_syncObj) {
        NSString *cachedFile = [self imageFilePathForURL:url];
        
        BOOL isDir = NO;
        if ([_fileManager fileExistsAtPath:cachedFile isDirectory:&isDir] && !isDir) {
            
            NSLog(@"Found cached iamge for URL: %@", url);
            
            UIImage *image = [UIImage imageWithContentsOfFile:cachedFile];
            
            if (image == nil) {
                NSLog(@"Broken image. Path: %@", cachedFile);
                
                [self deleteCachedFile:cachedFile];
            }
            
            return image;
        }
    }
    
    return nil;
}

- (void)addImage:(UIImage *)image forURL:(NSURL *)url {
    @synchronized(_syncObj) {
        [_fileManager tlt_enshureDirectoryExists:_cacheFolder];
        
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:[self imageFilePathForURL:url] atomically:NO];
        
        NSLog(@"Added to cache image for URL: %@", url);
    }
}

#pragma mark - private

+ (NSString *) cacheFolderPath {
    return [[TLTSystemFolders cachesPath] stringByAppendingPathComponent:@"ImagesCache"];
}

- (NSString *) imageFilePathForURL:(NSURL*)url {
    NSString *absoluteString = [url absoluteString];
    NSString *cachedImageName = [TLTMD5 md5: absoluteString];
    
    NSAssert(_cacheFolder != nil, @"Cache dir is nil");
    
    NSString *cachedImagePath = [_cacheFolder stringByAppendingPathComponent:cachedImageName];
    
    return cachedImagePath;
}

- (void) deleteCachedFile:(NSString*)path {
    NSError *err = nil;
    [_fileManager removeItemAtPath:path error:&err];
    
    NSAssert(err == nil, @"Removing of broken image failed. Path: %@", path);
}

@end
