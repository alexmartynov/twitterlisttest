//
//  TLTImageLoaderOperation.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTImageLoaderOperation.h"

@interface TLTImageLoaderOperation()

@property (nonatomic, strong) TLTImageLoaderTask *task;
@property (nonatomic, strong) TLTImageCache *imageCache;

@end

@implementation TLTImageLoaderOperation

+ (instancetype)imageLoaderOperationWithTask:(TLTImageLoaderTask *)aTask
                                  imageCache:(TLTImageCache*)anImageCache {
    return [[self alloc] initWithTask:aTask imageCache:anImageCache];
}

- (instancetype)initWithTask:(TLTImageLoaderTask *)aTask
                  imageCache:(TLTImageCache*)anImageCache {
    
    if (self = [super init]) {
        self.task = aTask;
        self.imageCache = anImageCache;
    }
    
    return self;
}

#pragma mark - NSOperation

- (void)main {
    
    NSURL *url = self.task.url;
    TLTImageLoaderCompletionHandler completionHandler = self.task.completionHandler;
    
    UIImage *cachedImage = [self.imageCache imageForURL:url];
    
    if (cachedImage != nil) {
        [self callbackToMainThreadWithImage:cachedImage error:nil completionHandler:completionHandler];
        return;
    }
        
    NSError *resultErr = nil;
    UIImage *resultImage = nil;
    
    if (self.isCancelled) {
        return;
    }
    
    NSData *imageData = [[NSData alloc] initWithContentsOfURL:url options:NSDataReadingUncached error:&resultErr];
    
    if (resultErr == nil) {
        if (self.isCancelled) {
            return;
        }
        //todo:: error handling
        resultImage = [UIImage imageWithData:imageData];
    
        [self.imageCache addImage:resultImage forURL:url];
    }
    
    [self callbackToMainThreadWithImage:resultImage error:resultErr completionHandler:completionHandler];
}

#pragma mark - private

- (void)callbackToMainThreadWithImage:(UIImage *)image
                                error:(NSError*)error
                    completionHandler:(TLTImageLoaderCompletionHandler)completionHandler {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.isCancelled) {
            completionHandler(image, error);
        }
    });
}

@end
