//
//  TLTImageLoaderOperation.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTImageLoaderTask.h"
#import "TLTImageCache.h"

@interface TLTImageLoaderOperation : NSOperation

+ (instancetype)imageLoaderOperationWithTask:(TLTImageLoaderTask *)aTask
                                  imageCache:(TLTImageCache*)anImageCache;

- (instancetype)initWithTask:(TLTImageLoaderTask *)aTask
                  imageCache:(TLTImageCache*)anImageCache;

@property (nonatomic, readonly, strong) TLTImageLoaderTask *task;

@end
