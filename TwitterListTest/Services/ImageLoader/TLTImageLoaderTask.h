//
//  TLTImageLoaderTask.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TLTImageLoaderCompletionHandler.h"

@class TLTImageLoader;

@interface TLTImageLoaderTask : NSObject

+ (instancetype) taskWithURL:(NSURL*)anURL
           completionHandler:(TLTImageLoaderCompletionHandler)completionHandler
                 imageLoader:(TLTImageLoader*)imageLoader;

- (instancetype) initWithURL:(NSURL*)anURL
           completionHandler:(TLTImageLoaderCompletionHandler)completionHandler
                 imageLoader:(TLTImageLoader*)imageLoader;

@property (nonatomic, readonly, strong) NSURL *url;
@property (nonatomic, readonly, strong) TLTImageLoaderCompletionHandler completionHandler;

- (void) start;

- (void) cancel;

@end
