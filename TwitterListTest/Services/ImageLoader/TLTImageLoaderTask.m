//
//  TLTImageLoaderTask.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTImageLoaderTask.h"

#import "TLTImageLoader.h"

@interface TLTImageLoaderTask()

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) TLTImageLoaderCompletionHandler completionHandler;
@property (nonatomic, strong) TLTImageLoader *imageLoader;

@end

@implementation TLTImageLoaderTask

+ (instancetype) taskWithURL:(NSURL*)anURL
           completionHandler:(TLTImageLoaderCompletionHandler)completionHandler
                 imageLoader:(TLTImageLoader*)imageLoader {
    return [[self alloc] initWithURL:anURL completionHandler:completionHandler imageLoader:imageLoader];
}

- (instancetype) initWithURL:(NSURL*)anURL
           completionHandler:(TLTImageLoaderCompletionHandler)completionHandler
                 imageLoader:(TLTImageLoader*)imageLoader {
    if (self = [super init]) {
        self.url = anURL;
        self.completionHandler = completionHandler;
        self.imageLoader = imageLoader;
    }
    
    return self;
}

- (void) start {
    [self.imageLoader startTask:self];
}

- (void) cancel {
    [self.imageLoader cancelTask:self];
}

@end
