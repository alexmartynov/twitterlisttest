//
//  TLTImageLoader.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/14/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTImageLoader.h"

#import "TLTImageCache.h"
#import "TLTImageLoaderOperation.h"

static const NSUInteger kTLTImageLoaderMaxNumberOfRunningTasks = 1;

@interface TLTImageLoader()

@property (nonatomic, strong) TLTImageCache *imageCache;
@property (nonatomic, strong) NSOperationQueue *loaderQueue;
@property (nonatomic, strong) NSObject *queueLockObj;

@end

@implementation TLTImageLoader

- (instancetype) init {
    if (self = [super init]) {
        self.imageCache = [TLTImageCache new];
        self.loaderQueue = [[NSOperationQueue alloc] init];
        self.loaderQueue.maxConcurrentOperationCount = kTLTImageLoaderMaxNumberOfRunningTasks;
        self.queueLockObj = [NSObject new];
    }
    
    return self;
}

#pragma mark - public

- (TLTImageLoaderTask *)taskForURL:(NSURL *)url
                 completionHandler:(TLTImageLoaderCompletionHandler)completionHandler {

    return [TLTImageLoaderTask taskWithURL:url completionHandler:completionHandler imageLoader:self];
}

- (void)startTask:(TLTImageLoaderTask *)task {
    @synchronized(self.queueLockObj) {
        TLTImageLoaderOperation *operation = [TLTImageLoaderOperation imageLoaderOperationWithTask:task imageCache:self.imageCache];
        [self.loaderQueue addOperation:operation];
    }
}

- (void)cancelTask:(TLTImageLoaderTask *)task {
    @synchronized(self.queueLockObj) {
        NSArray *operations = [self.loaderQueue operations];
        NSUInteger operationIdx = [operations indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            TLTImageLoaderOperation *op = (TLTImageLoaderOperation *) obj;
            if (op.task == task) {
                *stop = YES;
                return YES;
            }
            return NO;
        }];
        
        if (operationIdx != NSNotFound) {
            TLTImageLoaderOperation *operation = operations[operationIdx];
            [operation cancel];
        }
    }
}

@end
