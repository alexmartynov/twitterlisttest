//
//  TLTImageLoader.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/14/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TLTImageLoaderTask.h"
#import "TLTImageLoaderCompletionHandler.h"

@interface TLTImageLoader : NSObject

- (TLTImageLoaderTask *)taskForURL:(NSURL *)url
                 completionHandler:(TLTImageLoaderCompletionHandler)completionHandler;

- (void)startTask:(TLTImageLoaderTask *)task;

- (void)cancelTask:(TLTImageLoaderTask *)task;

@end
