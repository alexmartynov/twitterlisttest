//
//  TLTUserSettings.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTUserSettings.h"

static NSString * const kShowUserAvatarKey = @"kShowUserAvatarKey";

@implementation TLTUserSettings

- (instancetype) init {
    if (self = [super init]) {
        [self registerDefaults];
    }
    
    return self;
}

#pragma mark - public

- (BOOL) showUserAvatar {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kShowUserAvatarKey];
}

- (void) setShowUserAvatar:(BOOL)showUserAvatar {
    [[NSUserDefaults standardUserDefaults] setBool:showUserAvatar forKey:kShowUserAvatarKey];
    [self synchronize];
}

#pragma mark - private

- (void) synchronize {
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) registerDefaults {
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                 @(YES), kShowUserAvatarKey, nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
}

@end
