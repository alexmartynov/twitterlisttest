//
//  TLTUserSettings.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLTUserSettings : NSObject

@property (nonatomic, assign) BOOL showUserAvatar;

@end
