//
//  TLTTwitterPostDeserializer.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/13/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterPostDeserializer.h"

#import "TLTErrorUtils.h"
#import "TLTTwitterPost.h"
#import "TLTTwitterServiceError.h"

@interface TLTTwitterPostDeserializer() {
    NSDateFormatter *_dateFormatter;
}

@end

@implementation TLTTwitterPostDeserializer

- (instancetype)init{
    if (self = [super init]) {
        _dateFormatter = [TLTTwitterPostDeserializer makeDateFormatter];
    }
    return self;
}

- (BOOL)parseData:(NSData*)data outPosts:(NSArray**)outPosts outError:(NSError**)outErr {
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (dict == nil) {
        NSError *err = [TLTTwitterServiceError errorWithMessage:@"Data is not a valid JSON"];
        TLTSET_NSERROR_PTR(outErr, err);
        return NO;
    }
    
    if (dict[@"errors"] != nil) {
        NSError *err = [TLTTwitterServiceError errorWithErrorsDictionary:dict[@"errors"] message:@"Twitter API error"];
        TLTSET_NSERROR_PTR(outErr, err);
        return NO;
    }

    NSMutableArray *result = [NSMutableArray new];
    
    for (NSDictionary *status in dict[@"statuses"]) {
        NSDictionary *userDict = status[@"user"];
        
        int64_t userId = [userDict[@"id"] longLongValue];
        NSString *userName = userDict[@"name"];
        NSString *userScreenName = userDict[@"screen_name"];
        NSString *userAwatarURL = userDict[@"profile_image_url_https"];
        
        NSAssert(userName != nil, @"UserName can't be nil");
        NSAssert(userScreenName != nil, @"UserScreenName can't be nil");
        NSAssert(userAwatarURL != nil, @"UserAwatarURL can't be nil");

        TLTTwitterUser *user = [TLTTwitterUser userWithId:userId
                                                     name:userName
                                               screenName:userScreenName
                                                awatarURL:[NSURL URLWithString:userAwatarURL]];
        
        NSString *postText = status[@"text"];
        int64_t postId = [status[@"id"] longLongValue];
        NSDate *postDate = [_dateFormatter dateFromString:status[@"created_at"]];
        
        TLTTwitterPost *post = [TLTTwitterPost postWithId:postId text:postText date:postDate user:user];
        
        [result addObject:post];
    }
    
    *outPosts = result;
    return YES;
}

#pragma mark - private

+ (NSDateFormatter*)makeDateFormatter {
    NSDateFormatter *result = [NSDateFormatter new];
    
    //INFO: "created_at" = "Thu Aug 13 15:28:19 +0000 2015"
    result.dateFormat = @"EEE MMM dd HH:mm:ss Z yyyy";
    result.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    
    return result;
}

@end
