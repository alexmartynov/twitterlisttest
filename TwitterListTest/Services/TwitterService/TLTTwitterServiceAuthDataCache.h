//
//  TLTTwitterServiceAuthDataCache.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/19/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLTTwitterServiceAuthDataCache : NSObject

@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, readonly) BOOL hasAccessToken;

@end
