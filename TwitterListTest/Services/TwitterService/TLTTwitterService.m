//
//  TLTTwitterService.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/8/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TLTTwitterPost.h"
#import "TLTTwitterService.h"
#import "TLTTwitterURLBuilder.h"
#import "TLTTwitterPostDeserializer.h"
#import "TLTTwitterServiceAuthDataCache.h"

static NSString* const TLTTwitterServiceConsumerKey = @"yf2ElOIIvaTkH0Dgzj5bP4EJG";
static NSString* const TLTTwitterServiceConsumerSecret = @"rUTv9otlleBHhN91MFj2uFgG2qW9CPEhesI7rqQJezDjgMKsHH";

@interface TLTTwitterService()

@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, strong) NSObject *syncObj;
@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, strong) TLTTwitterServiceAuthDataCache *authDataCache;

@end

@implementation TLTTwitterService

- (instancetype) init {
    if (self = [super init]) {
        self.queue = dispatch_queue_create("com.TwitterListTest.TwitterService", NULL);
        self.syncObj = [NSObject new];
        self.authDataCache = [TLTTwitterServiceAuthDataCache new];
    }
    
    return self;
}

- (void)dealloc
{
    
}

#pragma mark - public

- (TLTTwitterServiceTask *)searchTaskWithQuery:(TLTTwitterSearchQuery *)query
                             completionHandler:(TLTTwitterServiceCompletionHandler)completionHandler {
    
    return [[TLTTwitterServiceTask alloc] initWithQuery:query
                                          authDataCache:self.authDataCache
                                           dispathQueue:self.queue
                                      completionHandler:completionHandler];
}

@end
