//
//  TLTTwitterServiceError.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/20/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kTLTTwitterServiceErrorDomain;

@interface TLTTwitterServiceError : NSError

+ (instancetype)errorWithMessage:(NSString *)msg;

+ (instancetype)errorWithErrorsDictionary:(NSDictionary *)errors message:(NSString *)msg;

- (instancetype)initWithErrorsDictionary:(NSDictionary *)errors message:(NSString *)msg;

@end
