//
//  TLTTwitterQuery.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/18/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TLTTWITTER_SEARCH_NO_ID (-1)

@interface TLTTwitterSearchQuery : NSObject

+ (instancetype) queryWithSearchString:(NSString *)aSearchString
                       numberOfResults:(NSUInteger)aNumberOfResults;

+ (instancetype) queryWithSearchString:(NSString *)aSearchString
                                 maxId:(int64_t)aMaxId
                               sinceId:(int64_t)aSinceId
                       numberOfResults:(NSUInteger)aNumberOfResults;

- (instancetype) initWithSearchString:(NSString *)aSearchString
                                maxId:(int64_t)aMaxId
                              sinceId:(int64_t)aSinceId
                      numberOfResults:(NSUInteger)aNumberOfResults;

@property (nonatomic, readonly, copy) NSString *searchString;
@property (nonatomic, readonly, assign) int64_t maxId;
@property (nonatomic, readonly, assign) int64_t sinceId;

@end
