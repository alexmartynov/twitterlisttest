//
//  TLTTwitterQuery.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/18/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterSearchQuery.h"

@interface TLTTwitterSearchQuery()

@property (nonatomic, copy) NSString *searchString;
@property (nonatomic, assign) int64_t maxId;
@property (nonatomic, assign) int64_t sinceId;
@property (nonatomic, assign) NSUInteger numberOfResults;

@end

@implementation TLTTwitterSearchQuery

+ (instancetype) queryWithSearchString:(NSString *)aSearchString
                       numberOfResults:(NSUInteger)aNumberOfResults {
    return [self queryWithSearchString:aSearchString
                                 maxId:TLTTWITTER_SEARCH_NO_ID
                               sinceId:TLTTWITTER_SEARCH_NO_ID
                       numberOfResults:aNumberOfResults];
}

+ (instancetype) queryWithSearchString:(NSString *)aSearchString
                                 maxId:(int64_t)aMaxId
                               sinceId:(int64_t)aSinceId
                       numberOfResults:(NSUInteger)aNumberOfResults {
    return [[self alloc] initWithSearchString:aSearchString maxId:aMaxId sinceId:aSinceId numberOfResults:aNumberOfResults];
}

- (instancetype) initWithSearchString:(NSString *)aSearchString
                                maxId:(int64_t)aMaxId
                              sinceId:(int64_t)aSinceId
                      numberOfResults:(NSUInteger)aNumberOfResults {
    if (self = [super init]) {
        self.searchString = aSearchString;
        self.maxId = aMaxId;
        self.sinceId = aSinceId;
        self.numberOfResults = aNumberOfResults;
    }
    return self;
}

@end
