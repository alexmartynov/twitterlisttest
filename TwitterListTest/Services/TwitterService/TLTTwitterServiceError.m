//
//  TLTTwitterServiceError.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/20/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterServiceError.h"

NSString * const kTLTTwitterServiceErrorDomain = @"com.TwitterListTest.TwitterService";

@interface TLTTwitterServiceError() {
    NSDictionary *_errorsDict;
}

@end

@implementation TLTTwitterServiceError

+ (instancetype)errorWithMessage:(NSString*)msg {
    return [self errorWithErrorsDictionary:nil message:msg];
}

+ (instancetype)errorWithErrorsDictionary:(NSDictionary *)errors message:(NSString *)msg {
    return [[self alloc] initWithErrorsDictionary:errors message:msg];
}

- (instancetype)initWithErrorsDictionary:(NSDictionary *)errors message:(NSString *)msg {
    NSMutableDictionary* userInfo = nil;
    
    if (msg != nil) {
        userInfo = [NSMutableDictionary dictionary];
        [userInfo setValue:msg forKey:NSLocalizedDescriptionKey];
    }
    
    if (self = [super initWithDomain:kTLTTwitterServiceErrorDomain code:-1 userInfo:userInfo]) {
        _errorsDict = errors;
    }
    
    return self;
}

#pragma mark - NSObject

- (NSString *)description {
    NSString *errorsDescription = (_errorsDict != nil) ? [NSString stringWithFormat:@" Errors: %@", _errorsDict] : @"";
    NSString *result = [NSString stringWithFormat:@"%@%@", [super description], errorsDescription];
    
    return result;
}

@end
