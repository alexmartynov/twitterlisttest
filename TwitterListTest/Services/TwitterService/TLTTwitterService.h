//
//  TLTTwitterService.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/8/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterSearchQuery.h"
#import "TLTTwitterServiceTask.h"
#import "TLTTwitterServiceCompletionHandler.h"

@interface TLTTwitterService : NSObject

- (TLTTwitterServiceTask *)searchTaskWithQuery:(TLTTwitterSearchQuery *)query
                             completionHandler:(TLTTwitterServiceCompletionHandler)completionHandler;

@end
