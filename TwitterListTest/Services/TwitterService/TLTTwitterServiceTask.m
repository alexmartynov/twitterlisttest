//
//  TLTTwitterServiceTask.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/18/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterServiceTask.h"

#import "TLTTwitterURLBuilder.h"
#import "TLTTwitterSearchQuery.h"
#import "TLTTwitterPostDeserializer.h"
#import "TLTTwitterServiceAuthDataCache.h"

static NSString* const TLTTwitterServiceConsumerKey = @"yf2ElOIIvaTkH0Dgzj5bP4EJG";
static NSString* const TLTTwitterServiceConsumerSecret = @"rUTv9otlleBHhN91MFj2uFgG2qW9CPEhesI7rqQJezDjgMKsHH";

@interface TLTTwitterServiceTask() {
    BOOL _isCanceled;
}

@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, strong) TLTTwitterSearchQuery *query;
@property (nonatomic, strong) TLTTwitterServiceAuthDataCache *authCache;
@property (nonatomic, strong) TLTTwitterServiceCompletionHandler completionHandler;
@property (nonatomic, strong) NSObject *syncObj;
@property (nonatomic, strong) NSURLSessionDataTask *downloadTask;

@end

@implementation TLTTwitterServiceTask

- (instancetype) initWithQuery:(TLTTwitterSearchQuery *)aQuery
                 authDataCache:(TLTTwitterServiceAuthDataCache *)anAuthCache
                  dispathQueue:(dispatch_queue_t)aQueue
             completionHandler:(TLTTwitterServiceCompletionHandler)anCompletionHandler {
    
    if (self = [super init]) {
        self.queue = aQueue;
        self.query = aQuery;
        self.authCache = anAuthCache;
        self.completionHandler = anCompletionHandler;
        self.syncObj = [NSObject new];
    }
    
    return self;
}

- (void) start {
    if (!self.authCache.hasAccessToken) {
        
        dispatch_barrier_async(self.queue, ^{
            if (self.authCache.hasAccessToken) {
                return;
            }
            
            [self authorizeWithCompletionHandler:^(NSString *token, NSError *err) {
                self.authCache.accessToken = token;
            }
                               waitForCompletion:YES];
        });
    }
    
    //todo:: handle auth fail
    //todo:: handle parse error
    dispatch_async(self.queue, ^{
        [self searchPostsWithQuery:self.query accessToken:self.authCache.accessToken completionHandler:^(NSArray *posts, NSError *err) {
            self.completionHandler(posts, err);
        }];
    });
}

- (void) cancel {
    @synchronized(self.syncObj) {
        _isCanceled = YES;
        [self.downloadTask cancel];
        self.downloadTask = nil;
    }
}

- (BOOL) isCanceled {
    BOOL result = NO;
    @synchronized(self.syncObj) {
        result = _isCanceled;
    }
    return result;
}

#pragma mark - private

+ (NSData*)postDataForAuthRequest {
    const NSString* kPostData = @"grant_type=client_credentials";
    
    return [kPostData dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSString*)authorizationHeader {
    NSString *keyStr = [NSString stringWithFormat:@"%@:%@", TLTTwitterServiceConsumerKey, TLTTwitterServiceConsumerSecret];
    NSData *keyData = [keyStr dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64EncodedStr = [keyData base64EncodedStringWithOptions:0];
    
    return [NSString stringWithFormat:@"Basic %@", base64EncodedStr];
}

- (void)authorizeWithCompletionHandler:(void (^)(NSString *token, NSError *err))completionHandler
                     waitForCompletion:(BOOL)shouldWait {

    //INFO: https://dev.twitter.com/oauth/application-only
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.twitter.com/oauth2/token"]];
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[[self class] authorizationHeader] forHTTPHeaderField:@"Authorization"];
    [request setHTTPBody:[[self class] postDataForAuthRequest]];
    
    __block BOOL isTaskCompleted = NO;
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error != nil) {
            completionHandler(nil, error);
        }
        else {
            //todo:: parse error
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSString *accessToken = dict[@"access_token"];
            
            completionHandler(accessToken, nil);
        }
        
        isTaskCompleted = YES;
    }];
    
    [dataTask resume];
    
    while (shouldWait && !isTaskCompleted) {
        CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0.2f, false);
    }
}

- (void)searchPostsWithQuery:(TLTTwitterSearchQuery *)query
                 accessToken:(NSString *)accessToken
           completionHandler:(void (^)(NSArray *posts, NSError *err))completionHandler {
    
    NSURL *searchURL = [TLTTwitterURLBuilder searchURLForQuery:query];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:searchURL];
    request.HTTPMethod = @"GET";
    
    NSString *bearer = [NSString stringWithFormat:@"Bearer %@", accessToken];
    [request setValue:bearer forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (self.canceled) {
            return;
        }
        
        if (error != nil) {
            completionHandler(nil, error);
        }
        else {
            TLTTwitterPostDeserializer *postDeserializer = [TLTTwitterPostDeserializer new];
            
            NSArray *posts = nil;
            NSError *parseErr = nil;
            [postDeserializer parseData:data outPosts:&posts outError:&parseErr];
            completionHandler(posts, parseErr);
        }
        
        @synchronized(self.syncObj) {
            self.downloadTask = nil;
        }
    }];
    
    @synchronized(self.syncObj) {
        self.downloadTask = dataTask;
        [self.downloadTask resume];
    }
}

@end
