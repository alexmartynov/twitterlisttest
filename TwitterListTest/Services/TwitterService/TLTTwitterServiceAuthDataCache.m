//
//  TLTTwitterServiceAuthDataCache.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/19/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterServiceAuthDataCache.h"

@interface TLTTwitterServiceAuthDataCache()

@property (nonatomic, copy) NSString *cachedAccessToken;
@property (nonatomic, strong) NSObject *syncObj;

@end

@implementation TLTTwitterServiceAuthDataCache

- (instancetype)init{
    if (self = [super init]) {
        self.syncObj = [NSObject new];
    }
    
    return self;
}

#pragma mark - public

- (NSString *)accessToken {
    NSString *result = nil;
    
    @synchronized(self.syncObj) {
        result = [self.cachedAccessToken copy];
    }
    
    return result;
}

- (void)setAccessToken:(NSString *)token {
    @synchronized(self.syncObj) {
        self.cachedAccessToken = token;
    }
}

- (BOOL)hasAccessToken {
    BOOL result = NO;
    
    @synchronized(self.syncObj) {
        result = (self.cachedAccessToken != nil);
    }
    
    return result;
}

@end
