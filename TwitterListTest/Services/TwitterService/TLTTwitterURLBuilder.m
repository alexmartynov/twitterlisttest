//
//  TLTTwitterURLBuilder.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/18/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterURLBuilder.h"

@implementation TLTTwitterURLBuilder

//INFO: https://api.twitter.com/1.1/search/tweets.json
+ (NSURL *)searchURLForQuery:(TLTTwitterSearchQuery *)query {

    NSMutableString *resultStr = [NSMutableString stringWithString:@"https://api.twitter.com/1.1/search/tweets.json"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"q"] = query.searchString;
    if (query.maxId != TLTTWITTER_SEARCH_NO_ID) {
        params[@"max_id"] = @(query.maxId);
    }
    if (query.sinceId != TLTTWITTER_SEARCH_NO_ID) {
        params[@"since_id"] = @(query.sinceId);
    }
    
    if ([params count] > 0) {
        [resultStr appendString:@"?"];
        [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSString *paramName = (NSString *)key;
            
            if ([obj isKindOfClass:[NSString class]]) {
                NSString *paramValue = [(NSString *)obj stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
                [resultStr appendFormat:@"&%@=%@", paramName, paramValue];
            } else if ([obj isKindOfClass: [NSNumber class]]) {
                NSNumber *paramValue = (NSNumber *)obj;
                [resultStr appendFormat:@"&%@=%d", paramName, paramValue.intValue];
            }
        }];
    }
    
    return [NSURL URLWithString:resultStr];
}

@end
