//
//  TLTTwitterServiceTask.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/18/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TLTTwitterServiceCompletionHandler.h"

@class TLTTwitterSearchQuery;
@class TLTTwitterServiceAuthDataCache;

@interface TLTTwitterServiceTask : NSObject

- (instancetype) initWithQuery:(TLTTwitterSearchQuery *)aQuery
                 authDataCache:(TLTTwitterServiceAuthDataCache *)anAuthCache
                  dispathQueue:(dispatch_queue_t)aQueue
             completionHandler:(TLTTwitterServiceCompletionHandler)anCompletionHandler;

@property (nonatomic, readonly, assign, getter=isCanceled) BOOL canceled;

- (void) start;

- (void) cancel;

@end
