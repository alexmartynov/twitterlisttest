//
//  TLTTwitterURLBuilder.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/18/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TLTTwitterSearchQuery.h"

@interface TLTTwitterURLBuilder : NSObject

+ (NSURL *)searchURLForQuery:(TLTTwitterSearchQuery *)query;

@end
