//
//  TLTCountdownLabel.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTCountdownLabel.h"

#import "TLTCancelToken.h"

static const NSTimeInterval kTLTCountdownLabelUpdateInterval = 1.0f;

@interface TLTCountdownLabel() {
    BOOL _isPaused;
    NSTimeInterval _countdownTime;
    NSTimeInterval _lastTimerTime;
}

@property (nonatomic, strong) NSTimer *countdownTimer;
@property (nonatomic, strong) TLTCancelToken *cancelToken;

@end

@implementation TLTCountdownLabel

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self subscribeToNotifications];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self subscribeToNotifications];
    }
    return self;
}

- (void)dealloc{
    [self unsubscribeFromNotifications];
    [self stopTimer:YES];
}

#pragma mark - public

- (void) startCountdownWithTimeInterval:(NSTimeInterval)aCountdownTime {
    [self stop];
    
    _countdownTime = aCountdownTime;
    [self resetText];
    
    if (self.hidesWhenStopped) {
        self.hidden = NO;
    }
    
    [self startTimer];
}

- (void) stop {
    [self stopTimer:YES];
    self.hidden = self.hidesWhenStopped;
}

#pragma mark - actions

- (void) didEnterBackgroundAction {
    if (self.countdownTimer != nil) {
        [self stopTimer:YES];
        _isPaused = YES;
    }
}

- (void) willEnterForegroundAction {
    if (_isPaused) {
        [self startTimer];
        _isPaused = NO;
    }
}

#pragma mark - private

- (void) subscribeToNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackgroundAction) name:UIApplicationDidEnterBackgroundNotification object: [UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForegroundAction) name:UIApplicationWillEnterForegroundNotification object: [UIApplication sharedApplication]];
}

- (void) unsubscribeFromNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) stopTimer:(BOOL)isCancelCallback {
    [self.countdownTimer invalidate];
    self.countdownTimer = nil;
    [self.cancelToken cancel];
    self.cancelToken = nil;
    _lastTimerTime = 0;
}

- (void) startTimer {
    //todo::NSTimer retains self, so dealloc will never be called
    _lastTimerTime = [[NSDate new] timeIntervalSinceReferenceDate];
    self.cancelToken = [TLTCancelToken new];
    self.countdownTimer = [NSTimer timerWithTimeInterval:kTLTCountdownLabelUpdateInterval
                                                  target:self
                                                selector:@selector(updateText)
                                                userInfo:nil
                                                 repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:self.countdownTimer forMode:NSRunLoopCommonModes];
}

- (void) resetText {
    self.text = [self textForInterval:_countdownTime];
}

- (void) updateText {
    NSTimeInterval newLastTimerTime = [[NSDate new] timeIntervalSinceReferenceDate];
    NSTimeInterval secondsPassed = newLastTimerTime - _lastTimerTime;
    _lastTimerTime = newLastTimerTime;
    
    if (secondsPassed < 0) {
        secondsPassed = 0;
    }
    
    if (secondsPassed > _countdownTime) {
        secondsPassed = _countdownTime;
    }
    
    _countdownTime = _countdownTime - secondsPassed;
    
    if (_countdownTime < 0) {
        _countdownTime = 0;
    }
    
    self.text = [self textForInterval:_countdownTime];
    [self setNeedsDisplay];
    
    if (_countdownTime < DBL_EPSILON) {
        [self stop];
        [self callbackOnDidFinishCounting:self.cancelToken];
    }
}

- (NSString *) textForInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    return [NSString stringWithFormat:@"%02ld", (long)ti];
}

- (void) callbackOnDidFinishCounting:(TLTCancelToken *)cancelToken {
    if (cancelToken.canceled) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (cancelToken.canceled) {
            return;
        }
        
        if ([self.delegate respondsToSelector:@selector(countdownLabelDidFinishCounting:)]) {
            [self.delegate countdownLabelDidFinishCounting:self];
        }
    });
}

@end
