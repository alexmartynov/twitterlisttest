//
//  TLTCancelToken.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/20/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTCancelToken.h"

@interface TLTCancelToken() {
    BOOL _isCanceled;
    NSObject *_syncObj;
}

@end

@implementation TLTCancelToken

- (instancetype) init {
    if (self = [super init]) {
        _isCanceled = NO;
        _syncObj = [NSObject new];
    }
    return self;
}

#pragma mark - public

- (BOOL)isCanceled {
    BOOL result = NO;
    
    @synchronized(_syncObj) {
        _isCanceled = result;
    }
    
    return result;
}

- (void) cancel {
    @synchronized(_syncObj) {
        _isCanceled = YES;
    }
}

@end
