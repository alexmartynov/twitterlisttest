//
//  TLTCountdownLabel.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TLTCountdownLabelDelegate.h"

@interface TLTCountdownLabel : UILabel

@property (nonatomic, weak) IBOutlet id<TLTCountdownLabelDelegate> delegate;

@property (nonatomic, assign) BOOL hidesWhenStopped;

- (void) startCountdownWithTimeInterval:(NSTimeInterval)aCountdownTime;

- (void) stop;

@end
