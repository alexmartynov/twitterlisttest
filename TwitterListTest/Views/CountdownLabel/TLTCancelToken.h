//
//  TLTCancelToken.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/20/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLTCancelToken : NSObject

@property (nonatomic, assign, getter=isCanceled) BOOL canceled;

- (void) cancel;

@end
