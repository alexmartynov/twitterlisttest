//
//  TLTCountdownLabelDelegate.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TLTCountdownLabel;

@protocol TLTCountdownLabelDelegate <NSObject>

@optional

- (void) countdownLabelDidFinishCounting:(TLTCountdownLabel*)label;

@end
