//
//  TLTSwitchTableViewCell.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLTSwitchTableViewCell : UITableViewCell

@property (strong, nonatomic, readonly) UILabel *switchTitle;
@property (strong, nonatomic, readonly) UISwitch *switchControl;
@property (strong, nonatomic) void (^valueChangedHandler)(BOOL newValue);

@end
