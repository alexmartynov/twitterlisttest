//
//  TLTSwitchTableViewCell.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/16/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTSwitchTableViewCell.h"

@interface TLTSwitchTableViewCell()

@property (strong, nonatomic) IBOutlet UILabel *switchTitle;
@property (strong, nonatomic) IBOutlet UISwitch *switchControl;

@end

@implementation TLTSwitchTableViewCell

#pragma mark - Actions

- (IBAction)switchValueChanged:(id)sender {
    if (self.valueChangedHandler != nil) {
        self.valueChangedHandler (self.switchControl.on);
    }
}

@end
