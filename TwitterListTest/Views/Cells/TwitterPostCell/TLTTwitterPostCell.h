//
//  TLTTwitterPostCell.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/8/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLTTwitterPost;
@class TLTImageLoader;

@interface TLTTwitterPostCell : UITableViewCell

- (void) configure:(TLTTwitterPost*)post imageLoader:(TLTImageLoader *)loader;
- (void) configure:(TLTTwitterPost*)post imageLoader:(TLTImageLoader *)loader displayProfileImage:(BOOL)shouldDisplayImage;

@end
