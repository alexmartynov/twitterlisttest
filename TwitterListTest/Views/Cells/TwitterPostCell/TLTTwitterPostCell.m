//
//  TLTTwitterPostCell.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/8/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterPostCell.h"

#import "TLTTwitterPost.h"
#import "TLTImageLoader.h"

@interface TLTTwitterPostCell() {
    TLTImageLoader *_imageLoader;
    CGFloat _imageViewDefaultWidth;
}

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *postTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *postDateLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthConstraint;

@property (nonatomic, strong) TLTImageLoaderTask *imageLoaderTask;

@end

@implementation TLTTwitterPostCell

- (void) awakeFromNib {
    [super awakeFromNib];
    
    _imageViewDefaultWidth = self.imageViewWidthConstraint.constant;
}

- (void) prepareForReuse {
    [super prepareForReuse];

    [self.imageLoaderTask cancel];
}

#pragma mark - public

- (void) configure:(TLTTwitterPost*)post imageLoader:(TLTImageLoader *)loader {
    
    [self configure:post imageLoader:loader displayProfileImage:YES];
}

- (void) configure:(TLTTwitterPost*)post imageLoader:(TLTImageLoader *)loader displayProfileImage:(BOOL)shouldDisplayImage {
    _imageLoader = loader;
    
    self.userNameLabel.text = post.user.name;
    self.postTextLabel.text = post.text;
    self.postDateLabel.text = [NSDateFormatter localizedStringFromDate:post.date dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
    
    if (shouldDisplayImage) {
        self.imageViewWidthConstraint.constant = _imageViewDefaultWidth;
        self.profileImageView.image = [UIImage imageNamed:@"twitter_post_placeholder"];
        [self startLoadImage:post];
    } else {
        self.profileImageView.image = nil;
        self.imageViewWidthConstraint.constant = 0.0f;
    }
}

#pragma mark - private

- (void) startLoadImage:(TLTTwitterPost*)post {
    if (post.user.awatarURL == nil) {
        NSLog(@"User %@ doesn't have avatar image", post.user.name);
        return;
    }
    
    self.imageLoaderTask = [_imageLoader taskForURL:post.user.awatarURL completionHandler:^(UIImage *image, NSError *err) {
        if (err == nil) {
            self.profileImageView.image = image;
        }
    }];
    
    [self.imageLoaderTask start];
}

@end
