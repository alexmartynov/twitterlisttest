//
//  AppDelegate.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/8/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "AppDelegate.h"

#import "TLTTwitterService.h"

@interface AppDelegate ()

@property (nonatomic, strong) UIWindow *mainWindow;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    return YES;
}

@end
