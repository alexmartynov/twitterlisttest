//
//  TLTTwitterUser.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/10/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterUser.h"

@interface TLTTwitterUser()

@property (nonatomic, assign) int64_t userId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *screenName;
@property (nonatomic, strong) NSURL *awatarURL;

@end

@implementation TLTTwitterUser

+ (instancetype)userWithId:(int64_t)anUserId
                      name:(NSString*)aName
                screenName:(NSString*)aScreenName
                 awatarURL:(NSURL*)anAwatarURL {
    return [[self alloc] initWithId:anUserId name:aName screenName:aScreenName awatarURL:anAwatarURL];
}

- (instancetype)initWithId:(int64_t)anUserId
                      name:(NSString*)aName
                screenName:(NSString*)aScreenName
                 awatarURL:(NSURL*)anAwatarURL {
    if (self = [super init]) {
        self.userId = anUserId;
        self.name = aName;
        self.screenName = aScreenName;
        self.awatarURL = anAwatarURL;
    }
    
    return self;
}

@end
