//
//  TLTTwitterPost.m
//  TwitterListTest
//
//  Created by Alex Martynov on 8/10/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import "TLTTwitterPost.h"

@interface TLTTwitterPost()

@property (nonatomic, assign) int64_t postId;

@property (nonatomic, copy) NSString *text;

@property (nonatomic, strong) NSDate *date;

@property (nonatomic, strong) TLTTwitterUser *user;

@end

@implementation TLTTwitterPost

+ (instancetype)postWithId:(int64_t)aPostId
                      text:(NSString*)aText
                      date:(NSDate*)aDate
                      user:(TLTTwitterUser*)anUser {
    return [[self alloc] initWithId:aPostId text:aText date:aDate user:anUser];
}

- (instancetype)initWithId:(int64_t)aPostId
                      text:(NSString*)aText
                      date:(NSDate*)aDate
                      user:(TLTTwitterUser*)anUser {
    if (self = [super init]) {
        self.postId = aPostId;
        self.text = aText;
        self.date = aDate;
        self.user = anUser;
    }
    
    return self;
}

@end
