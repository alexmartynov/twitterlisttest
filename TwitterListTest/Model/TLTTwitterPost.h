//
//  TLTTwitterPost.h
//  TwitterListTest
//
//  Created by Alex Martynov on 8/10/15.
//  Copyright (c) 2015 Alex Martynov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TLTTwitterUser.h"

@interface TLTTwitterPost : NSObject

+ (instancetype)postWithId:(int64_t)aPostId
                      text:(NSString*)aText
                      date:(NSDate*)aDate
                      user:(TLTTwitterUser*)anUser;

- (instancetype)initWithId:(int64_t)aPostId
                      text:(NSString*)aText
                      date:(NSDate*)aDate
                      user:(TLTTwitterUser*)anUser;

@property (nonatomic, readonly, assign) int64_t postId;

@property (nonatomic, readonly, copy) NSString *text;

@property (nonatomic, readonly, strong) NSDate *date;

@property (nonatomic, readonly, strong) TLTTwitterUser *user;

@end
